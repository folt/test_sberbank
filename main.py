import ujson
import utils


def main():
    with open('source.json', 'r') as dtf, \
            open('index.html', 'w') as htf:
        json = ujson.loads(dtf.read())
        res = []
        utils.populate_lists(json, res)
        htf.write(''.join(res))


if __name__ == '__main__':
    main()
