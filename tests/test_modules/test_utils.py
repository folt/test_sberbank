from utils import parse_css


def test_parse_css():
    """Проверка парсинга css"""
    tag = "p.my-class1.my-class2"
    data = ('p', ['class="my-class1 my-class2"'])
    assert data == parse_css(tag)
