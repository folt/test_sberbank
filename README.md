# Тестовое задание для Сбербанк.

Задание: конвертер json разметки в HTML

Выполнял: Васкевич Александр

Контакты: vaskevic.an@gmail.com, https://t.me/retrosky

Для запуска нужно установить виртуальное окружение.
В проекте используется пакетный мереджер poetry (https://python-poetry.org/)

Установить зависимости: 
`poetry install`

Запуск тестов:
`python -m pytest tests/`
