import re

re_id = re.compile('\#([^\.]+)')


def parse_css(tag):
    res = re_id.search(tag)
    attrs = []

    if res:
        css_id = res.group(1)
        tag = tag.replace('#' + css_id, '')
        attrs.append('id="{}"'.format(css_id))

    cls_css = tag.split('.')
    new_tag = cls_css.pop(0)

    if len(cls_css):
        attrs.append('class="{}"'.format(' '.join(cls_css)))

    return new_tag, attrs


def populate_lists(src, res):
    res.append('<ul>')

    for item in src:
        if isinstance(item, list):
            res.append('<li>')
            populate_lists(item, res)
            res.append('</li>')
        else:
            res.append('<li>')

            for k, v in item.items():
                t, attrs = parse_css(k)
                kc = '</{}>'.format(t)

                if len(attrs):
                    ko = '<{0} {1}>'.format(t, ' '.join(attrs))
                else:
                    ko = '<{}>'.format(t)

                if isinstance(v, list):
                    res.append(ko)
                    populate_lists(v, res)
                    res.append(kc)
                else:
                    res.append('{}{}{}'.format(ko, v, kc))

            res.append('</li>')

    res.append('</ul>')


__all__ = (
    'parse_css',
    'populate_lists',
)
